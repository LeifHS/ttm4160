package exercises.trafficlight;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class TrafficLight {
	
	private final GpioController gpio;
	private final GpioPinDigitalOutput redLED;
	private final GpioPinDigitalOutput yellowLED;
	private final GpioPinDigitalOutput greenLED;
	

	public TrafficLight(String title, boolean showYellow){

		gpio = GpioFactory.getInstance();
		
		if(showYellow) {
			redLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07, "RedCAR", PinState.LOW);
			yellowLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "YellowCAR", PinState.HIGH);
			greenLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_13, "GreenCAR", PinState.LOW);
		} else {
			redLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "RedPED", PinState.LOW);
			yellowLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12, "YellowPED", PinState.LOW);
			greenLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_14, "GreenPED", PinState.LOW);
		}

	}
	
	public void showGreen() {
		redLED.low();
		yellowLED.low();
		greenLED.high();

	}

	public void showRed() {
		yellowLED.low();
		greenLED.low();
		redLED.high();
	}

	public void showRedYellow() {
		greenLED.low();
		yellowLED.high();
		redLED.high();

	}

	public void showYellow() {
		greenLED.low();
		redLED.low();
		yellowLED.high();

	}

	public void switchAllOff() {
		greenLED.low();
		redLED.low();
		yellowLED.low();

	}
}     

