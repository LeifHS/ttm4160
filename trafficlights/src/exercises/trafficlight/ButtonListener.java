package exercises.trafficlight;

import runtime.Scheduler;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

public class ButtonListener {

	private final GpioController gpio;
	private final GpioPinDigitalInput myButton;
	
	public ButtonListener(final String[] events, final Scheduler s) {
		gpio = GpioFactory.getInstance();
		myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_11, PinPullResistance.PULL_UP);
		
		// create and register gpio pin listener
        myButton.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            	if(event.getState() == PinState.HIGH) {
            		s.addToQueueLast(events[0]);
            	}
            }
            
        });
		
		
	}
}
