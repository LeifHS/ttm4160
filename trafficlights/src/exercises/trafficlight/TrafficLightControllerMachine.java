package exercises.trafficlight;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;

public class TrafficLightControllerMachine implements IStateMachine {
	
	private static final String PEDESTRIAN_BUTTON_PRESSED = "Pedestrian Button";
	
	public static final String[] EVENTS = {PEDESTRIAN_BUTTON_PRESSED};
	
	private enum STATES {S0, S1, S2, S3, S4, S5}
	
	private Timer t1 = new Timer("t1");
	private Timer t2 = new Timer("t2");
	private Timer t3 = new Timer("t3");
	private Timer t4 = new Timer("t4");
	private Timer t5 = new Timer("t5");
	
	protected STATES state = STATES.S0;
	
	private TrafficLight cars = new TrafficLight("Cars", true);
	private TrafficLight pedestrians = new TrafficLight("Pedestrians", false);
	
	private static boolean buttonPressed;
	private static boolean initial;

	private final int lightNumber;
	
	public TrafficLightControllerMachine(int lightNumber) {
		this.lightNumber = lightNumber;
		// initial transition
		cars.showGreen();
		pedestrians.showRed();
		buttonPressed = false;
		initial = true;
	}
	
	public void startUDPListener(final Scheduler scheduler) {
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					DatagramSocket serverSocket = new DatagramSocket(4160);
					byte[] receiveData = new byte[1024];
					while(true) {
						DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
						serverSocket.receive(receivePacket);
						String received = new String( receivePacket.getData());   
						if(!received.startsWith("TEAM13;")) {
							System.err.println("Received invalid packet..."+received);
							continue;
						}
						String[] parts = received.split(";");
						if(parts.length < 2) {
							System.err.println("Received invalid packet...\n"+received);
							continue;
						}
						String event = parts[1];
						System.out.println("Received event: "+event+".");
						
						scheduler.addToQueueLast(event);
												
					}	
				} catch(IOException ex) {
					ex.printStackTrace();
				}
				
			}
		});
		th.start();
	}

	public int fire(String event, Scheduler scheduler) {
		if(state==STATES.S0) {
			if(event.equals("NOTIFY_LIGHT"+lightNumber)) {
				if(buttonPressed){
					buttonPressed = false;
					cars.showYellow();
					t1.start(scheduler, 1000);
					state = STATES.S1;
					return EXECUTE_TRANSITION;
				}else{
					state = STATES.S0;
					return EXECUTE_TRANSITION;
				}
			} else if(event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
				buttonPressed = true;
				state = STATES.S0;
				return EXECUTE_TRANSITION;
			}  
		} else if(state==STATES.S1) {
			if(event.equals("t1")){
				cars.showRed();
				t2.start(scheduler, 1000);
				state = STATES.S2;
				return EXECUTE_TRANSITION;
			}
		} else if(state==STATES.S2) {
			if(event.equals("t2")){
				pedestrians.showGreen();
				t3.start(scheduler, 1000);
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
		} else if(state==STATES.S3) {
			if(event.equals("t3")){
				pedestrians.showRed();
				t4.start(scheduler, 1000);
				state = STATES.S4;
				return EXECUTE_TRANSITION;
			}
		} else if(state==STATES.S4) {
			if(event.equals("t4")){
				cars.showRedYellow();
				t5.start(scheduler, 1000);
				state = STATES.S5;
				return EXECUTE_TRANSITION;
			}
		} else if(state==STATES.S5) {
			if(event.equals("t5")){
				cars.showGreen();
				state = STATES.S0;
				return EXECUTE_TRANSITION;
			}
		}
		return DISCARD_EVENT;
	}
	
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("arguments: <light_number>");
			return;
		}
		
		int lightNumber = Integer.parseInt(args[0]);
		
		TrafficLightControllerMachine stm = new TrafficLightControllerMachine(lightNumber);
		Scheduler s = new Scheduler(stm);
		
		ButtonListener bl = new ButtonListener(EVENTS, s);

		stm.startUDPListener(s);
		s.start();
	}

}
