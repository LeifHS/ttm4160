package exercises.trafficlight;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;

public class MultipleTrafficLightControllerMachine implements IStateMachine {
	
	private Timer t1 = new Timer("t1");
	private Timer t2 = new Timer("t2");
	
	private static boolean initial;
	
	private static long DELAY1 = 24000;
	private static long DELAY2 = 12000;
	
	public MultipleTrafficLightControllerMachine() {
		// initial transition
		initial = true;
		
		
	}
	
	private void notifyUDP(String event) {
		try {
			DatagramSocket clientSocket = new DatagramSocket(); 
			InetAddress address = InetAddress.getByName("192.168.251.255");
			String sentence = "TEAM13;"+event+";";
			byte[] sendData = sentence.getBytes();       
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, 4160);
			clientSocket.send(sendPacket);
			System.out.println("Sending packet: "+sentence);
			clientSocket.close();
		} catch( IOException ex ) {
			ex.printStackTrace();
		}
		
	}

	public int fire(String event, Scheduler scheduler) {
		if(event.equals("t1")){
			// Signal light 1 and  2
			notifyUDP("NOTIFY_LIGHT1");
			notifyUDP("NOTIFY_LIGHT2");
			// Restart the t1 timer
			t1.start(scheduler, DELAY1);
			return EXECUTE_TRANSITION;
			
		}
		else if(event.equals("t2")) {
			// Signal light 3
			notifyUDP("NOTIFY_LIGHT3");
			// Restart the t2 timer
			t2.start(scheduler, DELAY2);
			return EXECUTE_TRANSITION;
		}
		return DISCARD_EVENT;
	}
	
	public void start(Scheduler scheduler) {
		t1.start(scheduler, DELAY1);
		t2.start(scheduler, DELAY2);
	}
	
	public static void main(String[] args) {
		MultipleTrafficLightControllerMachine stm = new MultipleTrafficLightControllerMachine();
		Scheduler s = new Scheduler(stm);

		stm.start(s);
		s.start();
	}

}
